package Kith::SQL;

use strict;

BEGIN {
	use Exporter 'import';
	our @EXPORT_OK = qw($sql); # Export our $sql variable for later use
}
my $prefix = main::PREFIX();
use DBI;
our $sql = DBI->connect(
	"dbi:SQLite:dbname=". $prefix . "/kithairon.db",
	"",
	"", {RaiseError => 1, AutoCommit => 1});

1;
