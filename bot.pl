#!/usr/bin/perl

use strict;
use POE qw(Component::IRC);
use Getopt::Long;

use vars qw($botnick $server $botchan);

BEGIN {
        use Cwd qw( abs_path getcwd );
        use File::Basename;
	my %consts = ( CWD => getcwd(), PREFIX => dirname(abs_path($0)), );
	require constant;
	import constant(\%consts);
}

chdir PREFIX;
use lib PREFIX;

use Kith::Conf qw($botnick $nickpass $server $port $serverpass $botchan); # Grab the variables we need from the conf module
use Kith::Stats::Registration;
use Kith::Stats::Cards;
use Kith::Stats::Battle;

my $debug;
GetOptions ('debug' => \$debug);

if (!-e "config/bot.conf") {
	die("Error: Please make sure you have both \'sql.conf\' and \'bot.conf\' in this directory\n");
}

my $version = "0.0.1";

our %stats; # Hash for keeping stats locally.  This will probably get moved in a few days.

our $irc = POE::Component::IRC->spawn(
	nick     => $botnick,
	ircname  => "Kithairon RPG Bot",
	username => $botnick,
	server   => $server,
	password => $serverpass,
) or die "Oh noooo! $!";

POE::Session->create(
	package_states => [
		main => [ qw(_start irc_001 irc_public irc_msg irc_notice) ],
	],
	heap => { irc => $irc },
);

sub _start {
	my $heap = $_[HEAP];
	my $irc = $heap->{irc};
	$irc->yield( register => 'all' );
	$irc->yield( connect => { Flood => '1' } ); # disables the flood-throttle in POE - should come in handy
	return;
}

sub daemonize() {
	close STDIN;
	close STDOUT;
	close STDERR;
	open(STDIN, '>', '/dev/null');
	open(STDOUT, '>', '/dev/null');
	open(STDERR,  '>', '/dev/null');
	if(fork()) { exit(0) }
}

daemonize() if !defined($debug);
$poe_kernel->run();
exit 0;

sub irc_001 {
	my $sender = $_[SENDER];
	my $irc = $sender->get_heap();
	print "Connected to ", $irc->server_name(), "\n" if defined($debug);
	$irc->yield( join => $botchan );
	print "Joining $botchan\n" if defined($debug);
	return;
}

sub irc_public {
 	my ($sender, $who, $where, $what) = @_[SENDER, ARG0 .. ARG2];
	my $nick = ( split /!/, $who )[0];
	my $channel = $where->[0];
	print "$nick:$channel> $what\n" if defined($debug);
}
sub irc_notice {
	my ($who, $target, $msg) = @_[ARG0 .. ARG2];
	my $nick = (split /!/,$who)[0];
	print "NOTICE:$nick> $msg\n" if defined($debug);
	if ($nick eq "NickServ" && $msg =~ /msg nickserv identify/i) {
		$irc->yield(privmsg => NickServ => "identify $nickpass") if $nickpass ne "";
	}
}
sub irc_msg {
	my ($who, $msg) = @_[ARG0, ARG2];
	my $nick = (split /!/,$who)[0];
	print "$nick> $msg\n" if defined($debug);
	if ($msg =~ /^\.register$/i) {
		if (!chk_registered($nick)) {
			$irc->yield(privmsg => $nick => "[Register] Welcome to Kithairon, $nick.  To begin, you must choose a card pack.");
			$irc->yield(privmsg => $nick => "[Register] Each pack comes with a boost in whichever stat the name of the pack falls under.  Choose one of the following card packs by typing \037.pack packname\037");
			$irc->yield(privmsg => $nick => "[Register] \002Attack\002, \002spirit\002, or \002speed\002");
		}
		else {
			$irc->yield(privmsg => $nick => "[Error] You are already registered.");
		}
	}
	if ($msg =~ /^\.pack (attack|spirit|speed)$/i) {
		if (!chk_registered($nick)) {
			$irc->yield(privmsg => $nick => "[Register] You have chosen the $1 pack.  Please wait while your information is recorded.");
			register($nick,$1);
		}
		else {
			$irc->yield(privmsg => $nick => "[Error] You have already chosen your pack, and are already registered.");
		}
	}
	if ($msg =~ /^\.hand$/i) {
		if (chk_registered($nick)) {
			show_hand($nick);
		}
		else {
			$irc->yield(privmsg => $nick => "[Error] You need to be registered first.");
		}
	}
	if ($msg =~ /^\.battle (.+)$/i) {
		my $nick2 = $1;
		if ($nick ne $nick2) {
			if (chk_registered($nick) && chk_registered($nick2)) {
				if (!chk_battle($nick) && !chk_battle($nick2)) {
					initiate_challenge($nick,$nick2);
					$irc->yield(privmsg => $nick => "[Battle] You have requested a battle with \002$nick2\002.  This request will expire in \002one minute\002.");
					$irc->yield(privmsg => $nick2 => "[Battle] \002$nick\002 has requested a battle!  Type \002.accept\002 or \002.decline\002");
				}
				elsif (chk_battle($nick)) {
					$irc->yield(privmsg => $nick => "[Error] You are currently in a battle.");
				}
				elsif (chk_battle($nick2)) {
					$irc->yield(privmsg => $nick => "[Error] Your opponent is in a battle.");
				}
			}
			else {
				$irc->yield(privmsg => $nick => "[Error] You or your opponent may not be registered.");
			}
		}
	}
	if ($msg =~ /^\.accept$/i) {
		if (chk_battle($nick)) {
			if (chk_opponent($nick) ne "" && chk_turn($nick) eq "deciding") {
				my $nick2 = chk_opponent($nick);
				start_battle($nick,$nick2);
				$irc->yield(privmsg => $nick2 => "[Battle] Your opponent has accepted the challenge.  The battle will now begin.  Showing extended hand information.");
				$irc->yield(privmsg => $nick => "[Battle] The battle will now begin.  Showing extended hand information.");
				show_hand_ext($nick);
				show_hand_ext($nick2);
			}
		}
		elsif (!chk_battle($nick)) {
			$irc->yield(privmsg => $nick => "[Error] You are not currently in a battle.");
		}
	}
	if ($msg =~ /^\.decline$/i) {
		if (chk_battle($nick)) {
			if (chk_opponent($nick) ne "") {
				my $nick2 = chk_opponent($nick);
				stop_challenge($nick,$nick2);
				$irc->yield(privmsg => $nick => "[Battle] You have declined the challenge.");
				$irc->yield(privmsg => $nick2 => "[Battle] Your opponent has declined the challenge.");
			}
			else {
				$irc->yield(privmsg => $nick => "[Error] This is not your opponent.");
			}
		}
	}
}
