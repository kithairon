package Kith::Stats::Registration;

use strict;
use Kith::SQL '$sql';
use Exporter 'import';
BEGIN { 
	our @EXPORT = qw(register chk_registered); 
}
my $prefix = main::PREFIX();
my $query;

sub chk_registered($) {
	my $nick = shift;
	$query = $sql->prepare("SELECT 1 FROM players WHERE name=?");
	$query->execute($nick);
	$query->fetch();
	return ($query->rows > 0);
}
sub register($$) {
	my ($nick,$pack) = @_;
	$query = $sql->prepare("INSERT INTO players (name, pack) VALUES(?, ?)");
	$query->execute($nick,$pack);
	gen_pack($nick,$pack);
}
sub gen_pack($$) {
	my ($nick, $pack) = @_;
	my ($atk,$spr,$spd) = (5,5,5);
	$atk += 5 if $pack =~ /attack/i;
	$spr += 5 if $pack =~ /spirit/i;
	$spd += 5 if $pack =~ /speed/i;
	for (my $i = 1;$i <= 5;$i++) {
		$query = $sql->prepare("SELECT * FROM cards WHERE form=1 ORDER BY RANDOM()");
		$query->execute();
		my ($id,$name) = ('','');
		$query->bind_columns(\$id, \$name, undef, undef, undef);
		$query->fetch();
		$query = $sql->prepare("INSERT INTO cardstats (c_id,owner,name,hp,mhp,atk,spr,spd,exp,mexp,lvl) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
		$query->execute($id,$nick,$name,10,10,$atk,$spr,$spd,0,50,1);
	}
	$query = $sql->prepare("SELECT r_id,name FROM cardstats WHERE owner = '$nick' LIMIT 5");
	$query->execute();
	my $x = 1;
	while (my ($id,$name) = $query->fetchrow_array()) {
		my $upquery = $sql->prepare("UPDATE players SET hand".$x." = ? WHERE name = '$nick'");
		$upquery->execute($id);
		$x++;
	}
	amass_cards($nick);
	$query = $sql->do("UPDATE players SET status = 'idle' WHERE name = '$nick'");
	$main::irc->yield(privmsg => $nick => "[Register] Registration is complete.  To list your cards in hand, type \002.hand\002");
}
sub amass_cards($) {
	my ($nick) = shift;
	$query = $sql->prepare("SELECT r_id,name FROM cardstats WHERE owner = '$nick'");
	$query->execute();
	my $x = 1;
	my $ids;
	while (my ($id,$name) = $query->fetchrow_array()) {
		$ids .= $id." ";
	}
	$sql->do("UPDATE players SET cardsowned = '$ids' WHERE name = '$nick'");
}

1;
