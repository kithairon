package Kith::Stats::Battle;

use strict;
use Kith::SQL '$sql';
use Exporter 'import';
BEGIN {
	our @EXPORT = qw(initiate_challenge chk_battle chk_opponent chk_turn stop_challenge start_battle);
}
my $prefix = main::PREFIX();
my $query;

sub initiate_challenge($$) {
	my ($nick1,$nick2) = @_;
	$query = $sql->prepare("UPDATE players SET status = 'battling', opponent = ?, turn = 'challenging' WHERE name = ?");
	$query->execute($nick2,$nick1);
	$query = $sql->prepare("UPDATE players SET status = 'battling', opponent = ?, turn = 'deciding' WHERE name = ?");
	$query->execute($nick1,$nick2);
}

sub chk_battle($) {
	my $nick = shift;
	$query = $sql->prepare("SELECT status FROM players WHERE name = '$nick'");
	$query->execute();
	my ($status) = $query->fetchrow_array();
	return $status eq "battling";
}

sub chk_opponent($) {
	my $nick = shift;
	$query = $sql->prepare("SELECT opponent FROM players WHERE name = '$nick'");
	$query->execute();
	my ($opponent) = $query->fetchrow_array();
	return $opponent;
}
sub chk_turn($) {
	my $nick = shift;
	$query = $sql->prepare("SELECT turn FROM players WHERE name = '$nick'");
	$query->execute();
	my ($turn) = $query->fetchrow_array();
	return $turn;
}
sub start_battle($$) {
        my ($nick1,$nick2) = @_;
        $query = $sql->prepare("UPDATE players SET status = 'battling', opponent = ?, turn = 'waiting' WHERE name = ?");
        $query->execute($nick2,$nick1);
        $query = $sql->prepare("UPDATE players SET status = 'battling', opponent = ?, turn = 'waiting' WHERE name = ?");
        $query->execute($nick1,$nick2);
}
sub stop_challenge {
	my $nick = shift;
	my $nick2 = chk_opponent($nick);
	$query = $sql->prepare("UPDATE players SET status = 'idling', opponent = '', turn = '' WHERE name = ?");
	$query->execute($nick);
	$query->execute($nick2);
}	

1;
