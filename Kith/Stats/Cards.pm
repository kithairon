package Kith::Stats::Cards;

use strict;
use Kith::SQL '$sql';
use Exporter 'import';
BEGIN {
	our @EXPORT = qw(show_hand show_hand_ext);
}
my $prefix = main::PREFIX();
my $query;

sub show_hand($) {
	my ($nick) = shift;
	my $output;
	my @hand = $sql->selectrow_array("SELECT hand1,hand2,hand3,hand4,hand5 FROM players WHERE name = '$nick'");
	for (my $i = 0;$i < 5;$i++) {
		my ($cardname,$level) = $sql->selectrow_array("SELECT name,lvl FROM cardstats WHERE r_id = '$hand[$i]'");
		if (!defined $output) { $output = "$cardname - Lvl. \002$level\002 "; }
		else { $output .= "| $cardname - Lvl. \002$level\002 "; }
	}
	$main::irc->yield(privmsg => $nick => "[Hand] $output");
}

sub show_hand_ext($) {
	my ($nick) = shift;
        my $output;
        my @hand = $sql->selectrow_array("SELECT hand1,hand2,hand3,hand4,hand5 FROM players WHERE name = '$nick'");
        for (my $i = 0;$i < 5;$i++) {
                my ($cardname,$hp,$mhp,$level) = $sql->selectrow_array("SELECT name,hp,mhp,lvl FROM cardstats WHERE r_id = '$hand[$i]'");
                if (!defined $output) { $output = "$cardname - HP: \002$hp/$mhp\002 - lvl. \002$level\002 "; }
                else { $output .= "| $cardname - HP: \002$hp/$mhp\002 - lvl. \002$level\002 "; }
        }
        $main::irc->yield(privmsg => $nick => "[HandExt] $output");
}

1;
