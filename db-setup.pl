#!/usr/bin/perl -w
use strict;
use DBI;

BEGIN {
	use Cwd qw( abs_path getcwd );
	use File::Basename;
	my %consts = ( CWD => getcwd(), PREFIX => dirname(abs_path($0)), );
	require constant;
	import constant(\%consts);
}

chdir PREFIX;
use lib PREFIX;
use Kith::SQL '$sql';
my $query;
if (!table_exists("players")) {
	print "Creating players table...";
	$query = $sql->prepare("CREATE TABLE players (id INTEGER PRIMARY KEY,name TEXT,pack TEXT,hand1 INTEGER,hand2 INTEGER,hand3 INTEGER,hand4 INTEGER,hand5 INTEGER,cardsowned TEXT,status TEXT,opponent TEXT,turn TEXT)");
	$query->execute();
	print " done!\n";
}else{
	print "Table players exists, skipping.\n";
}
if (!table_exists("cardstats")) {
	print "Creating cardstats table...";
	$query = $sql->prepare("CREATE TABLE cardstats (r_id INTEGER PRIMARY KEY,c_id INTEGER,owner TEXT,name TEXT,hp INTEGER,mhp INTEGER,atk INTEGER,spr INTEGER,spd INTEGER,exp INTEGER, mexp INTEGER,lvl INTEGER)");
	$query->execute();
	print " done!\n";
}else{
	print "Table cardstats exists, skipping.\n";
}
if (!table_exists("cards")) {
	print "Creating cards table...";
	$query = $sql->prepare("CREATE TABLE cards (id INTEGER PRIMARY KEY,name TEXT,form INTEGER,evolve_id INTEGER,evolve_lvl INTEGER)");
	$query->execute();
	print " done!\n";
}else{
	print "Table cards exists, dropping old values for possible updates...";
	$sql->do("DROP TABLE cards");
	$sql->do("CREATE TABLE cards (id INTEGER PRIMARY KEY,name TEXT,form INTEGER,evolve_id INTEGER,evolve_lvl INTEGER)");
	print " done!\n";
}
print "Inserting cards";
open FILE, "sql/cards.sql" or die $!;
while (my $line = <FILE>) {
	if ($line !~ /^#/ && $line ne "\n") {
		print ".";
		$sql->do($line);
	}
}
print " done!\n";

sub table_exists {
	my @row = $sql->selectrow_array("SELECT name FROM sqlite_master WHERE type='table' AND name='$_[0]'");
	return (@row > 0);
}
