package Kith::Conf;

use strict;

BEGIN {
	use Exporter 'import';
	our @EXPORT_OK = qw($botnick $nickpass $server $port $serverpass $botchan); # This took me a good part of a Saturday to figure out
}

my $prefix = main::PREFIX();
use Config::Tiny;

my $botConfig = Config::Tiny->read( $prefix . '/config/bot.conf' );

# Variables for various things that become exported
our $botnick = $botConfig->{_}->{nickname};
our $nickpass = $botConfig->{_}->{nicknamepasswd};
our $server = $botConfig->{_}->{server};
our $port = $botConfig->{_}->{port};
our $botchan = $botConfig->{_}->{channel}; # had to change this line from $channel because we use the variable $channel later in the public event
our $serverpass = $botConfig->{_}->{serverpass};

1;
